﻿#include <iostream>

class Vector
{
private:
	double x, y, z, modulV;
	
public:
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	int Modul()
	{		
		modulV = sqrt(x * x + y * y + z * z);
		return modulV;
	}
	
};

int main()
{
	Vector ModulVector (5, 2, 5);
	std::cout << ModulVector.Modul() << std::endl;
}